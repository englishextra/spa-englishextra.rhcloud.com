module.exports = function (grunt) {
	grunt.initConfig({
		jshint: {
			all: ["diy/libs/spa-englishextra/js/bundle.js"]
		}
	});
	grunt.loadNpmTasks("grunt-contrib-jshint");
	grunt.registerTask("default", "jshint");
};
