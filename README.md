# spa-englishextra.rhcloud.com

The online demo of spa-englishextra

[![spa-englishextra.rhcloud.com](https://farm1.staticflickr.com/604/23891093315_932d45d17c_o.jpg)](https://spa-englishextra.rhcloud.com/)

## On-line

 - [the website](https://spa-englishextra.rhcloud.com/)

## Dashboard

<https://openshift.redhat.com/app/console/application/5803ecb67628e131820001b2-spa>

## Production Push URL

```
ssh://5803ecb67628e131820001b2@spa-englishextra.rhcloud.com/~/git/app.git/
```

## Remotes

 - [GitHub](https://github.com/englishextra/spa-englishextra.rhcloud.com)
 - [BitBucket](https://bitbucket.org/englishextra/spa-englishextra.rhcloud.com)
 - [GitLab](https://gitlab.com/englishextra/spa-englishextra.rhcloud.com)
